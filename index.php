<?php require_once './code.php'  ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S04 - Access Modifiers and Encapsulation</title>
</head>

<body>

	<div style="padding: 10px; border: 2px solid black; width: auto; display: inline-block;">
		<h1>Access Modifiers</h1>

		<h3>Building Variables</h3>
			<!-- <p><?//php echo $building->name ; ?></p> -->
			<!-- <p><?//php echo $building->floors ; ?></p> -->
			<!-- <p><?//php echo $building->address ; ?></p> -->

		<h3>Condominium Variables</h3>
			<!-- <p><?//php echo $condominium->address; ?></p> -->

	</div>

	<div style="padding: 10px; border: 2px none;"></div>

	<div style="padding: 10px; border: 2px solid black; width: auto; display: inline-block;">
		<h1>Encapsulation</h1>
			<p>
				The name of the condominium is <?php echo $condominium->getName(); ?>
			</p>

			<? //php $condominium->setName('Enzo Tower') ?>

			<p>
				The name of the condominium has been changed to <?php echo $condominium->getName(); ?>
			</p>
	</div>

	<div style="padding: 10px; border: 2px none;"></div>

	<!-- MINI ACTIVITY -->
	<div style="padding: 10px; border: 2px solid black; width: auto; display: inline-block;">
		<h1>Mini Activity</h1>
			<p>
				The building has <?php echo $building->getFloor(); ?> floors
			</p>
			<?php $building->setFloor(7) ?>
			<p>
				The building now has <?php echo $building->getFloor(); ?> floors
			</p>

	</div>

	<div style="padding: 10px; border: 2px none;"></div>

	<!-- ACTIVITY -->
	<div style="padding: 10px; border: 2px solid black; width: auto; display: inline-block;">
		<h1>Activity</h1>

		<h3>Building</h3>
			<p>
				The name of the building is <?php echo $building->getName(); ?>
			</p>
			<p>
				The <?php echo $building->getName(); ?> has <?php echo $building->getFloor(); ?> floors
			</p>
			<p>
				The <?php echo $building->getName(); ?> is located at <?php echo $building->getAddress(); ?> floors
			</p>
			<?php $building->setName("Caswynn Complex") ?>
			<p>
				The name of the building has been changed to: <?php echo $building->getName(); ?>
			</p>



		<h3>Condominium</h3>
			<p>
				The name of the building is <?php echo $condominium->getName(); ?>
			</p>
			<p>
				The <?php echo $condominium->getName(); ?> has <?php echo $condominium->getFloor(); ?> floors
			</p>
			<p>
				The <?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress(); ?> floors
			</p>
			<?php $condominium->setName("Enzo Tower") ?>
			<p>
				The name of the condominium has been changed to: <?php echo $condominium->getName(); ?>
			</p>

		<h3>Code Snippet</h3>
		<img src="./code.png">

	</div>


</body>
</html>