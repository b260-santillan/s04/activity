<?php 
	class Building {

		// Properties
		protected $name;
		protected $floors;
		protected $address;

		// Constructor
		public function __construct($name, $floors, $address) {
			$this->name = $name;
			$this->floors = $floors;
			$this->address = $address;
		} 

		// Getter
		public function getName() {
			return $this->name;
		}
		public function getFloor() {
			return $this->floors;
		}
		public function getAddress() {
			return $this->address;
		}


		// Setter
		public function setname($name) {
			$this->name = $name;
		}
		public function setFloor($floors) {
			$this->floors = $floors;
		}
		public function setAddress($address) {
			$this->address = $address;
		}
	}

	// child class
	class Condominium extends Building {
		
		/*	Getters and Setters
			- write the getName and setName after defining the name property
			- these methods serve as the intermediary in accesseing the object's properties
			- these methods defines whether an object's property can be accesed or changed.
			- they implement the encapsulation of an object
		*/

	}

	$building = new Building ('Caswynn', 8, 'Timog Avenuue, Quezon City, Philippines');

	$condominium = new Condominium ('Enzo Condominium', 5, 'Buendia Avenue, Makati City');

 ?>